h3. "Javascript jQuery Hotkeys Plugin":http://code.google.com/p/js-hotkeys/

Cross Browser Module (Works with Safari, Opera, Firefox, Chrome and IE)
The latest Version is 0.7.8

bq. jQuery.Hotkeys plugin lets you easily add and remove handlers for keyboard events anywhere in your code supporting almost any key combination. It takes one line of code to bind/unbind a hot key combination.
h4. Example:

* Binding 'Ctrl+c'
{{{
$(document).bind('keydown', 'Ctrl+c', fn);
}}}

* Unbinding 'Ctrl+c'
{{{
$(document).unbind('keydown', 'Ctrl+c', fn);
}}}

h4. More info
* "Public Git Repository at GitHub":http://github.com/tzuryby/jquery.hotkeys/tree/master
* "Live Demo":http://jshotkeys.googlepages.com/test-static-01.html
* "Read More...":http://code.google.com/p/js-hotkeys/wiki/about


h3. Tips
* Textarea绑定hotkey
{{{
//content 是一个textarea对象的ID
jQuery.hotkeys.add('command+s', { target: jQuery("#content")[0] }, function(){jQuery('button:submit').click()})
}}}
