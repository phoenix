git clone git://github.com/technoweenie/restful-authentication.git restful_authentication

cd restful_authentication

rm .gitignore
rm -rf .git/

cd ..
mv restful_authentication/ /path/to/project/vendor/plugins/

cd /path/to/project/

./script/generate authenticated user sessions

rake db:migrate

visit: localhost:3000/register
