h3. Rails

# 如果我是从来没有接触过Rails的Web开发人员，你怎么向我介绍Rails，并鼓励我使用Rails？（对Rails整体框架的了解及其他框架的对比）
# 简要说明一下MVC的优缺点，以及在Rails中是怎么体现的（MVC Web开发的理解）
# 在Model中添加这样一行代码：has_many :posts，运行时将会发生什么？为什么？（了解元编程方面的知识）
# 自己写过Rake task吗？说说对Rake task的理解（rake task）
# 假设有一简单的网站采用REST模式并通过script/generate生成代码。解释一下当用户通过POST提交了一份表单到'/images/1’会发生什么事？说出你所能想象的。（route、controller方面的知识）
# form_for 和 form_tag 的区别（view）
# 以往项目中常用的plugin有哪些？


h3. Ruby

# Ruby中是怎样达到类似Java中的Namespace特性的？（代码组织）
# 写一个简单的类定义，包括你你能想到的尽可能多的特性。
# 简要说明对Ruby Code blocks的理解。



h3. CSS

# CSS selector？


h3. Javascript

# 使用过的javascript框架？说说体会




