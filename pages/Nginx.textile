bq. Nginx
简介：软件七层交换，反向代理服务器。能够很好地支持虚拟主机，可配置性很强，可以按URL做负载均衡。我目前一直在用，大约能支持3～5万条并发连接。
价格：免费、开源
网址：http://wiki.codemongers.com/NginxChs（中文维基） 

h3. 安装(trouble)

{{{
./configure: error: the HTTP rewrite module requires the PCRE library.
You can either disable the module by using --without-http_rewrite_module
option, or install the PCRE library into the system, or build the PCRE library
statically from the source with nginx by using --with-pcre=<path> option.

wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-7.8.tar.gz
tar xzf pcre-7.8.tar.gz
cd pcre-7.8
./configure && make && sudo make install
}}}

{{{
  Configure summary
  + using system PCRE library
  + OpenSSL library is not used
  + md5 library is not used
  + sha1 library is not used
  + using system zlib library

  nginx path prefix: "/usr/local/nginx"
  nginx binary file: "/usr/local/nginx/sbin/nginx"
  nginx configuration prefix: "/usr/local/nginx/conf"
  nginx configuration file: "/usr/local/nginx/conf/nginx.conf"
  nginx pid file: "/usr/local/nginx/logs/nginx.pid"
  nginx error log file: "/usr/local/nginx/logs/error.log"
  nginx http access log file: "/usr/local/nginx/logs/access.log"
  nginx http client request body temporary files: "/usr/local/nginx/client_body_temp"
  nginx http proxy temporary files: "/usr/local/nginx/proxy_temp"
  nginx http fastcgi temporary files: "/usr/local/nginx/fastcgi_temp"
}}}

h3. Configure

rails conf http://brainspl.at/nginx.conf.txt


h4. location
{{{
location  /i/ {
        root  /spool/w3;
    }

A request for "/i/top.gif" will return the file "/spool/w3/i/top.gif". You can use variables in the argument. 
}}}

h3. nginx的worker_processes设为多少才合适？
{{{
搜索到原作者的话：
一般一个进程足够了，你可以把连接数设得很大。如果有SSL、gzip这些比较消耗CPU的工作，而且是多核CPU的话，可以设为和CPU的数量一样。或者要处理很多很多的小文件，而且文件总大小比内存大很多的时候，也可以把进程数增加，以充分利用IO带宽（主要似乎是IO操作有block）。

    As a general rule you need the only worker with large number of
    worker_connections, say 10,000 or 20,000.

    However, if nginx does CPU-intensive work as SSL or gzipping and
    you have 2 or more CPU, then you may set worker_processes to be equal
    to CPU number.

    Besides, if you serve many static files and the total size of the files
    is bigger than memory, then you may increase worker_processes to
    utilize a full disk bandwidth.

    Igor Sysoev 

经我实践配置，多cpu＋gzip＋N多小文件＋文件总大小大大超过内存 的环境（BBS啦～），设置为cpu的两倍较好。（不过一个nginx是4.3M噢）

#  轻 Says:
October 21st, 2007 at 06:25:04

在我的实际使用中(nginx+php-cgi)，还是觉得CPU*1为好

一个CPU开启多个worker_processes的话，会导致负载瞬间狂升至20+

相反一个CPU只开一个的话，会很稳!


嗯，这个要根据环境调节的。我的是8个CPU的服务器。。。另外，可以修改nginx的cc make参数，把gcc的-g（debug）参数去掉。可以大大减小编译后的bin大小。
}}}

h3. {{NginxXSendfile}}

h3. 链接
* "WhyUseIt":http://wiki.codemongers.com/NginxChsWhyUseIt
